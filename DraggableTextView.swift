//
//  DraggableTextView.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 7/13/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit
import RxGesture
import RxSwift

public protocol DraggableTextViewDelegate {
    func textViewLongPressed(longPressedView textView:DraggableTextView)
}

public class DraggableTextView: UITextView {
    
    var draggableTextViewDelegate: DraggableTextViewDelegate?
    private let disposeBag = DisposeBag()
    
    //Mark: - Intializers
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    init(frame textViewFrame: CGRect, delegate draggableTextViewDelegate: DraggableTextViewDelegate ) {
        self.draggableTextViewDelegate = draggableTextViewDelegate
        super.init(frame: textViewFrame, textContainer: nil)
        setUp()
    }
    
    convenience public init(frame textViewFrame: CGSize, delegate draggableTextViewDelegate: DraggableTextViewDelegate) {
        self.init(frame: textViewFrame, delegate: draggableTextViewDelegate)
    }
    
    //Mark: - Private methods
    private func setUp() {
        self.rx
            .anyGesture(
                (.longPress(), when: .began),
                (.pan(), when: .changed),
                (.pinch(), when: .changed)
            )
            .subscribe(onNext: { gesture in
                if let gesture = gesture as? UIPanGestureRecognizer {
                    let translation = gesture.translation(in: self.superview)
                    if let view = gesture.view {
                        self.center = CGPoint(x:view.center.x + translation.x,
                                              y:view.center.y + translation.y)
                    }
                    gesture.setTranslation(CGPoint.zero, in: self.superview)
                } else if let gesture = gesture as? UIPinchGestureRecognizer {
                    self.transform = CGAffineTransform(scaleX: gesture.scale, y: gesture.scale)
                } else {
                    self.draggableTextViewDelegate?.textViewLongPressed(longPressedView: self)
                }
            })
            .disposed(by: disposeBag)
        }
}
