//
//  MenuTableViewController.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 7/9/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public protocol MenuTableViewControllerDelegate {
    func menuList(_ menuList: MenuTableViewController, didSelectName name: MenuItem) -> Void
}

open class MenuTableViewController: UITableViewController, NavigationItemConfigurable {
    
    // MARK: - Injections
    var _menuItems: [MenuItem]
    var _delegate: MenuTableViewControllerDelegate?
    
    // MARK: - Intializers
    required public init?(coder aDecoder: NSCoder) {
        self._menuItems = []
        super.init(coder: aDecoder)
    }
    
    // MARK: - View Lifecycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationItem()
    }
    
    // MARK: - Instance Properties
    public var menuItems: [MenuItem] {
        get { return self._menuItems }
        set { self._menuItems = newValue }
    }
    
    public var delegate: MenuTableViewControllerDelegate? {
        get { return self._delegate }
        set { self._delegate = newValue }
    }
    
    //MARK: - NavigationItemConfigurable protocol implementation
    func didTapCloseButton(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource
    override open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuItemCell", for: indexPath) as! MenuItemTableViewCell
        cell.menuItemImage.image = menuItems[indexPath.row].itemImage
        cell.menuItemName.text = menuItems[indexPath.row].menuItemType.rawValue;
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.menuList(self, didSelectName: menuItems[indexPath.row])
    }
}
