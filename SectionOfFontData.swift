//
//  FontData.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/30/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import Foundation
import RxDataSources

struct SectionOfFontData {
    var fonts: [String]
    var fontfamily: String
}
extension SectionOfFontData: SectionModelType {
    var items: [String] {
        return fonts
    }
    
    typealias Item = String
    
    init(original: SectionOfFontData, items: [String]) {
        self = original
        self.fonts = items
    }
}
