//
//  ColorType.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/16/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import Foundation

public enum ColorType: Int {
    case TextViewTextColor, TextViewBackGroundColor
}
