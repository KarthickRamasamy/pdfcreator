//
//  DocumentCreatorHomeTests.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 8/19/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import KIF
@testable import DocumentCreatorVC

class DocumentCreatorHomeTests: BaseUITests {

    func testAddTextView() {
       backToRoot()
       tapShowMenuButton()
       expectToSeeMenu()
       expectToSeeTextViewMenuOption()
       tapOnAddNewTextView()
    }
}
