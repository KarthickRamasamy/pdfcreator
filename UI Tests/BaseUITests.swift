//
//  BaseUITests
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 8/19/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.

import KIF

class BaseUITests: KIFTestCase {
  override func beforeAll() {
    super.beforeAll()
  }

  override func beforeEach() {
    super.beforeEach()
  }
}

extension BaseUITests {
    func tapButton(_ buttonName: String) {
        tester().tapView(withAccessibilityLabel: buttonName)
    }
    
    func backToRoot() {
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            navigationController.popToRootViewController(animated: false)
        }
    }
}
