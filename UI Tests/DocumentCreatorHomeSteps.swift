//
//  DocumentCreatorHomeSteps.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 8/19/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import Foundation
import Nimble
@testable import DocumentCreatorVC

extension DocumentCreatorHomeTests {
    func expectToSeeAddButton() {
        tester().waitForView(withAccessibilityLabel: "showMenu")
    }
    
    func expectToSeeMenu() {
         tester().waitForView(withAccessibilityLabel: "menu")
    }
    
    func expectToSeeMenuItemWithTitle(_ title: String, atRow row: NSInteger) {
        let menuItemCell = tester().waitForCell(at: IndexPath(row: row, section: 0), inTableViewWithAccessibilityIdentifier: "menu") as! MenuItemTableViewCell
        expect(menuItemCell.menuItemName.text) == title
    }
    
    func tapOnAddNewTextView() {
        let indexPath = IndexPath(row: 0, section: 0)
        tester().tapRow(at: indexPath, inTableViewWithAccessibilityIdentifier: "menu")
    }
    
    func tapShowMenuButton() {
        tester().tapView(withAccessibilityLabel: "showMenu")
    }
    
    func expectToSeeNewTextView() {
        tester().waitForView(withAccessibilityLabel: DocumentCreatorDraggableTextView.textViewAccessibilityLabel)
    }
    
    func expectToSeeTextViewMenuOption() {
        expectToSeeMenuItemWithTitle(MenuItemType.TextView.rawValue, atRow: 0)
    }
    
}
