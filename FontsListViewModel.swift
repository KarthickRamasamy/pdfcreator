//
//  FontsListViewModel.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/30/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import RxCocoa

class FontsListViewModel {
    func getFonts() -> Observable<[SectionOfFontData]> {
        return Observable.create { (observer) -> Disposable in
            let familyNames = UIFont.familyNames
            var fontData:[SectionOfFontData] = []
            for familyName in familyNames {
                let fonts = UIFont.fontNames(forFamilyName: familyName)
                let font = SectionOfFontData(fonts: fonts, fontfamily: familyName)
                fontData.append(font)
            }
            observer.onNext(fontData)
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
