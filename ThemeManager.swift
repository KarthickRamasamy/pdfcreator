//
//  ThemeManager.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 7/22/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

enum ThemeManager {
    
    static func applyTheme() {
        
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.barTintColor = UIColor(colorLiteralRed: 0/255, green: 113/255, blue: 255/255, alpha: 1)
        
    }
}
