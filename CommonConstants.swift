//
//  NavigationBarButton.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/16/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

struct CommonConstants {
    
    static var navigationBarButtonFrame: CGRect {
        return CGRect(x: 0, y: 0, width: 0, height: 0)
    }
}
