//
//  TVPropertyChangeRootVC.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 8/23/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit
import ChromaColorPicker
import RxSwift
import RxGesture

public protocol TextViewPropertyChangerDelegate {
    func didChooseTextColor(color: UIColor, colorType: ColorType, textView: DraggableTextView)
}

public class TextViewPropertyChangerViewController:UIViewController, NavigationItemConfigurable {
    
    // MARK: - Outlets
    @IBOutlet weak var propertySelector: UISegmentedControl!
    @IBOutlet weak var colorPicker: ChromaColorPicker!
    @IBOutlet weak var selectedFont: UILabel!
    
    //MARK: - Private variables
    let disposeBag = DisposeBag()
    
    //Mark: - View Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationItem()
        setupSelectedFontAction()
        colorPicker.padding = 0
        colorPicker.addTarget(self, action: #selector(colorSelected), for: .editingDidEnd)
        
    }
    
    // MARK: - Instance Properties
    var _delegate: TextViewPropertyChangerDelegate?
    public var delegate: TextViewPropertyChangerDelegate? {
        get {
            return self._delegate
        }
        set {
            self._delegate = newValue
        }
    }
    
    var _selectedTextView: UITextView?
    public var selectedTextView: UITextView? {
        get {
            return self._selectedTextView
        }
        set {
            self._selectedTextView = newValue
        }
    }
    
    //Mark: - Private methods
    @objc private func colorSelected(colorPicker: ChromaColorPicker) {
        if let textView = selectedTextView as? DraggableTextView, let selectedProperty = ColorType(rawValue: propertySelector.selectedSegmentIndex) {
            delegate?.didChooseTextColor(color: colorPicker.currentColor, colorType: selectedProperty, textView: textView)
        }
    }
    
    //Mark: - NavigationItemConfigurable implementation
    func didTapCloseButton(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupSelectedFontAction() {
        selectedFont.rx
            .tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] _ in
                
                
                guard let strongSelf = self else { return }
                let fontName = strongSelf.selectedTextView?.font?.fontName
                // show the task details view controller
                guard let fontsListViewController = strongSelf.storyboard?.instantiateViewController(withIdentifier: "FontsListViewController") as? FontsListViewController else {
                    fatalError("TaskDetailsViewController not found")
                }
                strongSelf.navigationController?.pushViewController(fontsListViewController, animated: true)
                fontsListViewController.subject.onNext(fontName!)
                fontsListViewController.subject.subscribe(onNext :{ [weak self] fontName in
                    let currentFontSize = self?.selectedTextView?.font?.pointSize
                    let font = UIFont(name: fontName, size: currentFontSize!)
                    self?.selectedTextView?.font =  font!
                    self?.selectedFont.text = fontName
                }).addDisposableTo(strongSelf.disposeBag)
            }).disposed(by: disposeBag)
    }
    
    private func setupPropertyChangeObserver() {
        propertySelector.rx.selectedSegmentIndex.map { index -> UIColor in
            switch index {
                case ColorType.TextViewTextColor.rawValue :
                      return (self.selectedTextView?.textColor)!
                case ColorType.TextViewBackGroundColor.rawValue :
                      return (self.selectedTextView?.backgroundColor)!
                default:
                      return UIColor.black
            }
        }.subscribe(onNext:{ color in
                self.colorPicker.adjustToColor(color)
          }).addDisposableTo(disposeBag)
    }
}
