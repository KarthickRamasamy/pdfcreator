//
//  DocumentCreatorDraggableTextView.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 8/15/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

public class DocumentCreatorDraggableTextView: DraggableTextView {
    
    //Mark: - Intializers
    override init(frame textViewFrame: CGRect, delegate textViewDelegate: DraggableTextViewDelegate) {
        super.init(frame: textViewFrame, delegate: textViewDelegate)
        setUp()
    }
    
    convenience init(parent parentView: UIView, size textViewSize: CGSize, delegate textViewDelegate: DraggableTextViewDelegate) {
        let textViewframe = CGRect(x: 100, y: 100, width: textViewSize.width, height: textViewSize.height)
        self.init(frame: textViewframe, delegate: textViewDelegate)
        setUp()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    //Mark: - Properties
    public static var textViewAccessibilityLabel: String? {
        get {
            return self.accessibilityLabel()
        }
    }
    
    //Mark: - Private methods
    private func setUp() -> Void {
        self.backgroundColor = UIColor.brown
        self.textColor = UIColor.black
        self.tintColor = UIColor.cyan
        self.font = UIFont.systemFont(ofSize: 15)
        self.accessibilityLabel = "textView"
    }
}
