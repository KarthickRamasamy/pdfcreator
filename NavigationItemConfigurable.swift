//
//  NavigationItemConfigurable.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/17/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

@objc protocol NavigationItemConfigurable: class {
    func didTapCloseButton(sender: UIBarButtonItem)
}

extension NavigationItemConfigurable where Self: UIViewController {
    
    func configureNavigationItem() {
        let closeButton = CloseButton(frame: CommonConstants.navigationBarButtonFrame)
        closeButton.addTarget(self, action: #selector(didTapCloseButton(sender:)), for: .touchUpInside)
        let barButtonCustomView = UIBarButtonItem(customView: closeButton)
        self.navigationItem.setRightBarButtonItems([barButtonCustomView], animated: true)
    }
}
