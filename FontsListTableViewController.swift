//
//  FontsListTableView.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/29/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class FontsListViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let fontsListViewModel = FontsListViewModel()
    private let familyNames = UIFont.familyNames
    private var selectedFont: String?
    let subject = PublishSubject<String>()
    let disposeBag = DisposeBag()

    let dataSource = RxTableViewSectionedReloadDataSource<SectionOfFontData>()
    
    init(selectedFont: String?) {
        self.selectedFont = selectedFont
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      //  selectedFont1.subscribe({_ in }).addDisposableTo(disposeBag)
        
        dataSource.configureCell = { (_, tv, ip, fontName) in
            let cell = tv.dequeueReusableCell(withIdentifier: "Cell", for: ip)
            cell.textLabel?.font = UIFont(name: fontName, size: 14)
            cell.textLabel?.text = fontName
            return cell
        }
        
        dataSource.titleForHeaderInSection = { ds, index in
            return ds.sectionModels[index].fontfamily
        }
        
        fontsListViewModel.getFonts()
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        setupTableViewBinding()
        
        subject.subscribe{
            print($0)
            }
            .addDisposableTo(disposeBag)
        
    }

    private func setupTableViewBinding() {
        tableView.rx
            .itemSelected
            .map { indexPath in
                return (indexPath, self.dataSource[indexPath])
            }
            .subscribe(onNext: { indexPath, fontName in
                print("indexPath \(indexPath.section) --- \(indexPath.row)")
                self.subject.onNext(fontName)
                self.navigationController?.popViewController(animated: true)
            })
            .disposed(by: disposeBag)
    }
    
//     override func numberOfSections(in tableView: UITableView) -> Int {
//        return familyNames.count
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return UIFont.fontNames(forFamilyName: familyNames[section]).count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
//        let list = UIFont.fontNames(forFamilyName: familyNames[indexPath.section])
//        let fontName: String = list[indexPath.row]
//        cell.textLabel?.font = UIFont(name: fontName, size: 14)
//        cell.textLabel?.text = fontName
////        print("familyname   \(familyNames[indexPath.section]) ---- fontname   \(fontName)")
//        if selectedFont ==  fontName {
//            cell.accessoryType = .checkmark
//        } else {
//            cell.accessoryType = .none
//        }
//        return cell
//    }
//    
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let list = UIFont.fontNames(forFamilyName: familyNames[section])
//        print("Total familynames \(familyNames) -- fonts")
//        let familyName = familyNames[section]
//        return familyName
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
//        let list = UIFont.fontNames(forFamilyName: familyNames[indexPath.section])
//        let fontName: String = list[indexPath.row]
//        selectedFont = fontName
//        fontSelected.onNext(fontName)
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        tableView.cellForRow(at: indexPath)?.accessoryType = .none
//    }

}
