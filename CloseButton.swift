//
//  CloseButton.swift
//  DocumentCreatorVC
//
//  Created by karthickramasamy on 9/16/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

class CloseButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setImage(UIImage(named: "close.png"), for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
