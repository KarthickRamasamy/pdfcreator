//
//  PropertyChangeBuilder.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 8/23/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

public class TextViewPropertyChanger {
    
    public class func instantiatePropertyChangerController(delegate: TextViewPropertyChangerDelegate, textView: UITextView) -> UINavigationController {
        let bundle = Bundle(for: TextViewPropertyChanger.self)
        let storyboard = UIStoryboard(name: String(describing: TextViewPropertyChanger.self), bundle: bundle)
        let navigationController = storyboard.instantiateInitialViewController() as! PropertyChangerNavigationController
        let propertyChanger = navigationController.topViewController as! TextViewPropertyChangerViewController
        propertyChanger.delegate = delegate
        propertyChanger.selectedTextView = textView
        return navigationController
    }
}
