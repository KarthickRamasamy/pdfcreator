//
//  MenuTableNavigationController.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 7/10/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

class MenuTableNavigationController: UINavigationController {

    // MARK: - Intializers
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.modalPresentationStyle = .formSheet
    }
}
