//
//  MenuBuilder.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 7/10/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

open class MenuBuilder {
    
    open class func instantiateMenuController(_ menuItems: [MenuItem], _ delegate: MenuTableViewControllerDelegate) -> UINavigationController {
        let bundle = Bundle(for: MenuBuilder.self)
        let storyboard = UIStoryboard(name: "Menu", bundle: bundle)
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let menuViewController = navigationController.topViewController as! MenuTableViewController
        menuViewController.menuItems = menuItems
        menuViewController.delegate = delegate
        return navigationController
    }
}
