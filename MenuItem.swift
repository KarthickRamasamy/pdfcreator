//
//  MenuItem.swift
//  Menu
//
//  Created by Karthick Ramasamy on 6/29/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

public class MenuItem {
    
    var menuItemType: MenuItemType
    var itemImage: UIImage
    
    //Mark: - Intializers
    init(itemType menuItemType: MenuItemType, image itemImage: UIImage) {
        self.menuItemType = menuItemType
        self.itemImage = itemImage
    }
}
