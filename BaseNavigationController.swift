//
//  BaseNavigationController.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 7/22/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

public class BaseNavigationController: UINavigationController {

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.tintColor = UIColor.white
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
