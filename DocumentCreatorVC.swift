//
//  DocumentCreatorVC.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 6/28/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

class DocumentCreatorVC: UIViewController {

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    @IBAction func showMenu(_ sender: Any) {
        let menuItems = [MenuItem(itemType: MenuItemType.TextView,image: #imageLiteral(resourceName: "editText"))]
        let menuViewController = MenuBuilder.instantiateMenuController(menuItems, self)
        self.present(menuViewController, animated: true, completion: nil)
    }
}

extension DocumentCreatorVC: MenuTableViewControllerDelegate {
    func menuList(_ menuList: MenuTableViewController, didSelectName menuItem: MenuItem) {
        if menuItem.menuItemType == MenuItemType.TextView {
            self.dismiss(animated: true, completion: {
                var drggableViewSize: CGSize
                if self.traitCollection.verticalSizeClass == .regular && self.traitCollection.horizontalSizeClass == .regular {
                    drggableViewSize = CGSize(width: 250, height: 250)
                }
                else {
                     drggableViewSize = CGSize(width: 100, height: 100)
                }
                let draggableTextView = DocumentCreatorDraggableTextView(parent: self.view, size: drggableViewSize, delegate: self)
                self.view.addSubview(draggableTextView)
            })
        }
    }
}

extension DocumentCreatorVC: DraggableTextViewDelegate, TextViewPropertyChangerDelegate {
    func didChooseTextColor(color: UIColor, colorType: ColorType, textView: DraggableTextView) {
        switch colorType {
        case .TextViewBackGroundColor:
              textView.backgroundColor = color
        case .TextViewTextColor:
            textView.textColor = color
        }
    }
    
    func textViewLongPressed(longPressedView textView: DraggableTextView) {
       let textViewPropertyChanger = TextViewPropertyChanger.instantiatePropertyChangerController(delegate: self, textView: textView)
       self.present(textViewPropertyChanger, animated: true, completion: nil)
    }
}
