//
//  ViewController.swift
//  DocumentCreatorVC
//
//  Created by Karthick Ramasamy on 6/28/17.
//  Copyright © 2017 Karthick Ramasamy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .red

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: MenuTableViewControllerDelegate {
    func menuList(_ menuList: MenuTableViewController, didSelectName name: MenuItem) {
        
    }
}

